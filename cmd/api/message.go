package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gaiax/didcommconnector/internal/config"
	intErr "gaiax/didcommconnector/internal/errors"
	"gaiax/didcommconnector/protocol"
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Summary		Receives a DIDComm message
// @Schemes
// @Description	Receives a DIDComm message
// @Tags			Message
// @Accept			json
// @Produce		json
// @Param			message	body		didcomm.Message	true	"Message"
// @Success		200	"OK"
// @Failure		400	"Bad Request"
// @Failure		500	"Internal Server Error"
// @Router			/message/receive  [post]
func (app *application) ReceiveMessage(context *gin.Context) {

	// get body of request
	bodyBytes, err := io.ReadAll(context.Request.Body)
	if err != nil {
		context.String(http.StatusBadRequest, "Error reading request body")
		return
	}
	bodyString := string(bodyBytes)

	// handle message
	packMsg, err := protocol.HandleMessage(bodyString, app.mediator)
	if err != nil {
		if errors.Is(err, intErr.ErrUnpackingMessage) {
			context.Status(http.StatusBadRequest)
			return
		} else {
			context.Status(http.StatusInternalServerError)
			return
		}
	}

	// answer request
	if packMsg != "" {
		var jsonMap map[string]interface{}
		err = json.Unmarshal([]byte(packMsg), &jsonMap)
		if err != nil {
			context.String(http.StatusInternalServerError, "Error marshaling message")
			return
		}

		context.JSON(http.StatusOK, jsonMap)
	} else {
		context.String(http.StatusOK, "")
	}
}

func (app *application) InvitationMessage(context *gin.Context) {
	mediator := app.mediator
	oob := protocol.NewOutOfBand(mediator)

	msg, err := oob.Handle()
	if err != nil {
		context.String(http.StatusInternalServerError, "Error handling out of band")
	} else {
		msg64 := base64.StdEncoding.EncodeToString([]byte(msg))
		oob := fmt.Sprintf("%s?_oob=%s", config.CurrentConfiguration.Url, msg64)
		context.String(http.StatusOK, oob)
	}
}
