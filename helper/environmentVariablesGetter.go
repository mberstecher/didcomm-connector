package helper

import (
	"fmt"

	"github.com/spf13/viper"
)

type EnvironmentVariable string

const (
	CASSANDRA_USERNAME EnvironmentVariable = "CASSANDRA_USERNAME"
	CASSANDRA_PASSWORD EnvironmentVariable = "CASSANDRA_PASSWORD"
	CASSANDRA_HOST     EnvironmentVariable = "CASSANDRA_HOST"
	CASSANDRA_PORT     EnvironmentVariable = "CASSANDRA_PORT"
	CASSANDRA_KEYSPACE EnvironmentVariable = "CASSANDRA_KEYSPACE"
	APP_PORT           EnvironmentVariable = "APP_PORT"
)

type DatabaseConfig struct {
	Username string `mapstructure:"CASSANDRA_USERNAME"`
	Password string `mapstructure:"CASSANDRA_PASSWORD"`
	Endpoint string `mapstructure:"CASSANDRA_ENDPOINT"`
	Port     int    `mapstructure:"CASSANDRA_PORT"`
	Keyspace string `mapstructure:"CASSANDRA_KEYSPACE"`
}

func (env EnvironmentVariable) GetEnvironmentVariable() string {
	switch env {
	case CASSANDRA_USERNAME:
		return getEnvironmentVariable(string(CASSANDRA_USERNAME))
	case CASSANDRA_PASSWORD:
		return getEnvironmentVariable(string(CASSANDRA_PASSWORD))
	case CASSANDRA_HOST:
		return getEnvironmentVariable(string(CASSANDRA_HOST))
	case CASSANDRA_PORT:
		return getEnvironmentVariable(string(CASSANDRA_PORT))
	case CASSANDRA_KEYSPACE:
		return getEnvironmentVariable(string(CASSANDRA_KEYSPACE))

	case APP_PORT:
		return getEnvironmentVariable(string(APP_PORT))

	default:
		return "Invalid environment variable key"
	}
}

func initViper() {
	viper.AddConfigPath(".")
	viper.SetConfigName("app")
	viper.SetConfigType("env")
}

func GetDatabaseConfig() (config DatabaseConfig, err error) {
	initViper()
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}

func getEnvironmentVariable(key string) (value string) {

	initViper()
	err := viper.ReadInConfig()

	if err != nil {
		panic(err)
	}

	value = viper.GetString(key)
	if value == "" {
		fmt.Println("No value for the environment variable '", key, "' found.")
	}
	return
}
