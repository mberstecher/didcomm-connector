package connectionmanager

import (
	"errors"
	"gaiax/didcommconnector/internal/config"
	"gaiax/didcommconnector/mediator/database"
)

type ConnectionManager struct {
	database database.Adapter
}

func NewConnectionManager(database database.Adapter) *ConnectionManager {
	return &ConnectionManager{
		database: database,
	}
}

var ERROR_PROTOCOL_NOT_SUPPORTED = errors.New("protocol not supported")
var ERROR_INTERNAL = errors.New("internal error")
var ERROR_CONNECTION_ALREADY_EXISTS = errors.New("connection already exists")

func (c *ConnectionManager) CreateConnectionInvitation(protocol string, remoteDid string, topic string, properties map[string]string) (err error) {
	switch config.CurrentConfiguration.CloudForwarding.Protocol {
	case config.HTTP:
		if protocol != config.HTTP {
			return ERROR_PROTOCOL_NOT_SUPPORTED
		}
	case config.NATS:
		if protocol != config.NATS {
			return ERROR_PROTOCOL_NOT_SUPPORTED
		}
	case "hybrid":
		if protocol != config.HTTP && protocol != config.NATS {
			return ERROR_PROTOCOL_NOT_SUPPORTED
		}
	default:
		return ERROR_PROTOCOL_NOT_SUPPORTED
	}
	isMediated, err := c.database.IsMediated(remoteDid)
	if err != nil {
		return ERROR_INTERNAL
	}
	if isMediated {
		return ERROR_CONNECTION_ALREADY_EXISTS
	}
	err = c.database.AddMediatee(database.Mediatee{RemoteDid: remoteDid, Protocol: protocol, Topic: topic, Properties: properties})
	if err != nil {
		return ERROR_INTERNAL
	}
	return nil
}
