package main

import (
	"gaiax/didcommconnector/didcomm"
	"gaiax/didcommconnector/internal/config"
	connectionmanager "gaiax/didcommconnector/mediator/connectionManager"
	"gaiax/didcommconnector/mediator/database"
	"gaiax/didcommconnector/protocol"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Summary	Get connections
// @Schemes
// @Description	Returns a list with the existing connections
// @Tags			Connections
// @Accept			json
// @Produce		json
// @Success		200	{array}	database.Mediatee
// @Failure		500	"Internal Server Error"
// @Router			/connections [get]
func (app *application) GetConnections(context *gin.Context) {
	logTag := "/connections [get]"
	config.Logger.Info(logTag, "Start", true)
	connections, err := app.mediator.Database.GetMediatees()
	if err != nil {
		config.Logger.Error(logTag, "Error", err)
		context.Status(http.StatusInternalServerError)
	}
	config.Logger.Info(logTag, "End", true)
	context.JSON(http.StatusOK, connections)
}

// @Summary	Get connection
// @Schemes
// @Description	Returns a connection
// @Tags			Connections
// @Accept			json
// @Produce		json
// @Param			did	path		string	true	"DID"
// @Success		200	{object}	database.Mediatee
// @Failure		204	"No object found"
// @Failure		500	"Internal Server Error"
// @Router			/connections/{did} [get]
func (app *application) GetConnection(context *gin.Context) {
	logTag := "/connections/{did} [get]"
	did := context.Param("did")
	config.Logger.Info(logTag, "did", did, "Start", true)
	connection, err := app.mediator.Database.GetMediatee(did)
	if err != nil {
		config.Logger.Error(logTag, "Error", err)
		context.Status(http.StatusInternalServerError)
	}
	config.Logger.Info(logTag, "End", true)
	context.IndentedJSON(http.StatusOK, connection)
}

// @Summary	Create a new connection
// @Schemes
// @Description	Creates a new connection
// @Tags			Connections
// @Accept			json
// @Produce		json
// @Param			connection	body		database.MediateeBase	true	"Connection"
// @Success		201			{object}	string
// @Failure		400			"Bad Request"
// @Failure		423			"Locked"
// @Failure		500			"Internal Server Error"
// @Router			/connections [post]
func (app *application) CreateConnection(context *gin.Context) {
	logTag := "/connections [post]"
	config.Logger.Info(logTag, "Start", true)
	context.Header("Content-Type", "application/json")

	var mediateeBase database.MediateeBase
	err := context.ShouldBindJSON(&mediateeBase)

	if err != nil {
		_ = app.SendPr(context, protocol.PR_INVALID_REQUEST, err)
		context.Status(http.StatusBadRequest)
		return
	}

	isBlocked, err := app.mediator.Database.IsBlocked(mediateeBase.RemoteDid)
	if err != nil {
		_ = app.SendPr(context, protocol.PR_INTERNAL_SERVER_ERROR, err)
		context.Status(http.StatusInternalServerError)
		return
	}
	if isBlocked {
		_ = app.SendPr(context, protocol.PR_DID_BLOCKED, err)
		context.Status(http.StatusLocked)
		return
	}

	err = app.mediator.ConnectionManager.CreateConnectionInvitation(mediateeBase.Protocol, mediateeBase.RemoteDid, mediateeBase.Topic, mediateeBase.Properties)
	if err != nil {
		if err == connectionmanager.ERROR_PROTOCOL_NOT_SUPPORTED {
			_ = app.SendPr(context, protocol.PR_PROTOCOL_NOT_SUPPORTED, err)
			context.Status(http.StatusBadRequest)
		} else if err == connectionmanager.ERROR_INTERNAL {
			_ = app.SendPr(context, protocol.PR_INTERNAL_SERVER_ERROR, err)
			context.Status(http.StatusInternalServerError)
		} else if err == connectionmanager.ERROR_CONNECTION_ALREADY_EXISTS {
			_ = app.SendPr(context, protocol.PR_ALREADY_CONNECTED, err)
			context.Status(http.StatusInternalServerError)
		} else {
			_ = app.SendPr(context, protocol.PR_INTERNAL_SERVER_ERROR, err)
			context.Status(http.StatusInternalServerError)
		}
		return

	}
	oob := protocol.NewOutOfBand(app.mediator)
	invitation, err := oob.Handle()
	if err != nil {
		config.Logger.Error(logTag, "Error", err)
		context.Status(http.StatusInternalServerError)
		return
	}
	config.Logger.Info(logTag, "End", true)
	context.String(http.StatusCreated, invitation)
}

// @Summary	Deletes a connection
// @Schemes
// @Description	Deletes a connection
// @Tags			Connections
// @Accept			json
// @Produce		json
// @Param			did	path string	true	"DID"
// @Success		200			"OK"
// @Failure		500			"Internal Server Error"
// @Router			/connections/{did} [delete]
func (app *application) DeleteConnection(context *gin.Context) {
	logTag := "/connections/{did} [delete]"
	did := context.Param("did")
	config.Logger.Info(logTag, "did", did, "Start", true)
	err := app.mediator.Database.DeleteMediatee(did)
	if err != nil {
		config.Logger.Error(logTag, "Error", err)
		context.Status(http.StatusInternalServerError)
		return
	}
	config.Logger.Info(logTag, "End", true)
	context.Status(http.StatusOK)

}

// @Summary	Blocks connection
// @Schemes
// @Description	Blocks connection
// @Tags			Connections
// @Accept			json
// @Produce		json
// @Param			did	path	string	true	"Did to be blocked"
// @Success		200	"OK"
// @Failure		500	"Internal Server Error"
// @Router			/connections/block/{did} [post]
func (app *application) BlockConnection(context *gin.Context) {
	logTag := "/connections/block/{did} [post]"
	did := context.Param("did")
	config.Logger.Info(logTag, "did", did, "Start", true)

	err := app.mediator.Database.BlockMediatee(did)
	if err != nil {
		config.Logger.Error(logTag, "Error", err)
		context.Status(http.StatusInternalServerError)
		return
	}
	config.Logger.Info(logTag, "End", true)
	context.Status(http.StatusOK)
}

// @Summary	Unblock existing connection
// @Schemes
// @Description	Blocks existing connection
// @Tags			Connections
// @Accept			json
// @Produce		json
// @Param			did	path	string	true	"Did to be unblocked"
// @Success		200	"OK"
// @Failure		500	"Internal Server Error"
// @Router			/connections/unblock/{did} [post]
func (app *application) UnblockConnection(context *gin.Context) {
	logTag := "/connections/unblock/{did} [post]"
	did := context.Param("did")
	config.Logger.Info(logTag, "did", did, "Start", true)
	err := app.mediator.Database.UnblockMediatee(did)
	if err != nil {
		config.Logger.Error(logTag, "Error", err)
		context.Status(http.StatusInternalServerError)
		return
	}
	config.Logger.Info(logTag, "End", true)
	context.Status(http.StatusOK)
}

// @Summary	Checks if a remoteDid is blocked
// @Schemes
// @Description	Checks if a remoteDid is blocked
// @Tags			Connections
// @Accept			json
// @Produce		json
// @Param			did	path		string	true	"Did"
// @Success		200	{object}	boolean
// @Failure		500	"Internal Server Error"
// @Router			/connections/isblocked/{did} [get]
func (app *application) IsBlocked(context *gin.Context) {
	logTag := "/connections/isblocked/{did} [get]"
	did := context.Param("did")

	config.Logger.Info(logTag, "did", did, "Start", true)
	ib, err := app.mediator.Database.IsBlocked(did)
	if err != nil {
		config.Logger.Error(logTag, "Error", err)
		context.Status(http.StatusInternalServerError)
		return
	}
	config.Logger.Info(logTag, "End", true)
	context.IndentedJSON(http.StatusOK, ib)
}

func (app *application) SendPr(context *gin.Context, pr didcomm.Message, err error) error {
	context.Header("Content-Type", "application/json")
	if err != nil {
		config.Logger.Error("Problem Report", "err", err)
	}
	packMsg, err := app.mediator.PackPlainMessage(pr)
	if err != nil {
		config.Logger.Error("Problem Report", "err", err)
		context.Status(http.StatusBadRequest)
		return err
	}
	context.String(http.StatusBadRequest, packMsg)
	return nil
}
