package protocol

import (
	"encoding/json"
	"errors"
	"fmt"
	"gaiax/didcommconnector/internal/config"
	"gaiax/didcommconnector/mediator"
	"gaiax/didcommconnector/mediator/database"
	"net/url"
	"sync"

	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
)

type outgoingNatsMessage struct {
	Properties map[string]string `json:"properties"`
	Message    []byte            `json:"message"`
}

type incomingNatsMessage struct {
	Properties map[string]string `json:"properties"`
	Message    []byte            `json:"message"`
}

func SendMessage(message string, mediatee *database.Mediatee) error {

	switch config.CurrentConfiguration.CloudForwarding.Protocol {
	case config.HTTP:
		return sendCloudEvent(message, mediatee)
	case config.NATS:
		return sendCloudEvent(message, mediatee)
	case config.HYBRID:
		// implement hybrid mode if cloud event provider supports it
		return errors.New("hybrid mode not supported: message will not be sent")
	default:
		return errors.New("unknown cloud forwarding mode")
	}
}

func ReceiveMessage(mediator *mediator.Mediator) {

	topic := config.CurrentConfiguration.CloudForwarding.Nats.Topic

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, topic)
	if err != nil {
		config.Logger.Error("unable to connect to cloud event provider", "msg", err)
	}

	defer client.Close()

	// Use a WaitGroup to wait for a message to arrive
	wg := sync.WaitGroup{}
	wg.Add(1)

	config.Logger.Info("Receiving cloud events", "topic", topic)

	err = client.Sub(func(event event.Event) {

		config.Logger.Info("Received cloud event", "context", event.Context)
		config.Logger.Info("Data", "context", string(event.DataEncoded))

		var incomingMessage incomingNatsMessage
		err := json.Unmarshal(event.DataEncoded, &incomingMessage)
		if err != nil {
			config.Logger.Error("error while unmarshalling received nats message")
			return
		}
		unpacked, err := mediator.UnpackMessage(string(incomingMessage.Message))
		if err != nil {
			config.Logger.Error("error while unpacking received nats message")
			return
		}
		mediatee, err := mediator.Database.GetMediatee(*unpacked.From)
		if err != nil {
			config.Logger.Error("error getting mediatee", "err", err)
			return
		}
		packedMsg, err := HandleMessage(string(incomingMessage.Message), mediator)
		if err != nil {
			config.Logger.Error("error while handling received nats message")
			return
		}
		if packedMsg != "" {
			if err != nil {
				config.Logger.Error("error getting mediatee", "err", err)
				return
			}
			// send message to cPCM (cloud)
			err = SendMessage(packedMsg, mediatee)
			if err != nil {
				config.Logger.Error("unable to send message to cloud", "err", err)
				return
			}
		}

	})
	if err != nil {
		config.Logger.Error("Error in subscription of cloud event", "msg", err)
	}

	// Wait for a message to come in
	wg.Wait()
}

func sendCloudEvent(message string, mediatee *database.Mediatee) (err error) {
	topic := mediatee.Topic
	if topic == "" {
		topic = "default-http"
	}
	client, err := cloudeventprovider.NewClient(cloudeventprovider.Pub, topic)
	if err != nil {
		config.Logger.Error("Can not create cloudevent client", "msg", err)
		return
	}
	defer client.Close()

	sourceUrl, err := url.JoinPath(config.CurrentConfiguration.CloudForwarding.Http.Url)

	config.Logger.Info(fmt.Sprintf("message to send as cloud event: %s", message))

	dataToSend := outgoingNatsMessage{
		Properties: mediatee.Properties,
		Message:    json.RawMessage(message),
	}

	dataJson, err := json.Marshal(dataToSend)
	if err != nil {
		config.Logger.Error("failed to marshal data to json", "msg", err)
		return
	}
	event, err := cloudeventprovider.NewEvent(sourceUrl, "application/json", dataJson)
	if err != nil {
		config.Logger.Error("failed to create cloud event", "msg", err)
		return
	}

	if err = client.Pub(event); err != nil {
		config.Logger.Error("failed to send cloud event", "msg", err)
		return
	}

	config.Logger.Info("published cloud event", "topic", topic)

	return
}
