#!/bin/bash

echo "Starting to execute SH script..."

CREATE_KEYSPACE="CREATE KEYSPACE IF NOT EXISTS ${CASSANDRA_KEYSPACE} WITH REPLICATION = {
  'class' : 'SimpleStrategy', 
  'replication_factor' : 1 }; "
USE_KEYSPACE="USE ${CASSANDRA_KEYSPACE}; "
CREATE_CONNECTIONS_TABLE="CREATE TABLE IF NOT EXISTS connections (
  did text, is_mediated boolean, is_denied boolean, added TIMESTAMP, PRIMARY KEY (did)) "

CQL="$CREATE_KEYSPACE$USE_KEYSPACE$CREATE_CONNECTIONS_TABLE"

echo "$CQL"

while ! cqlsh cassandra_db -u "${CASSANDRA_USERNAME}" -p "${CASSANDRA_PASSWORD}" -e 'describe cluster' ; do
  echo "Waiting for main instance to be ready..."
  sleep 5
done

cqlsh cassandra_db -u "${CASSANDRA_USERNAME}" -p "${CASSANDRA_PASSWORD}" -e "${CQL}"

# for cql_file in ./cql/*.cql;
# do
#  cqlsh cassandra_db -u "${CASSANDRA_USERNAME}" -p "${CASSANDRA_PASSWORD}" -f "${cql_file}" ;
#  echo "Script ""${cql_file}"" executed"
# done

echo "Execution of SH script is finished"
echo "Stopping temporary database instance"