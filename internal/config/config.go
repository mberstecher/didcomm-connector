package config

import (
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/viper"
)

// constants for environment and mode
const (
	ENV_DEV  = "DEV"
	ENV_PROD = "PROD"

	HTTP   = "http"
	NATS   = "nats"
	HYBRID = "hybrid"
)

type TemplateConfiguration struct {
	Env      string `mapstructure:"env"`
	LogLevel string `mapstructure:"logLevel"`
	Port     int    `mapstructure:"port"`
	Url      string `mapstructure:"url"`

	DidComm struct {
		ResolverUrl        string `mapstructure:"resolverUrl"`
		IsMessageEncrypted bool   `mapstructure:"messageEncrypted"`
	} `mapstructure:"didcomm"`

	CloudForwarding struct {
		Protocol string `mapstructure:"protocol"`
		Nats     struct {
			Url   string `mapstructure:"url"`
			Topic string `mapstructure:"topic"`
		} `mapstructure:"nats"`
		Http struct {
			Url string `mapstructure:"url"`
		} `mapstructure:"http"`
	} `mapstructure:"messaging"`

	Database struct {
		InMemory bool   `mapstructure:"inMemory"`
		Host     string `mapstructure:"host"`
		Port     int    `mapstructure:"port"`
		User     string `mapstructure:"user"`
		Password string `mapstructure:"password"`
		Keyspace string `mapstructure:"keyspace"`
		DBName   string `mapstructure:"dbName"`
	} `mapstructure:"db"`

	LoggerFile *os.File
}

var CurrentConfiguration TemplateConfiguration
var Logger *slog.Logger
var env string

func LoadConfig() error {
	setDefaults()
	readConfig()
	if err := viper.Unmarshal(&CurrentConfiguration); err != nil {
		return err
	}

	setEnvironment()
	if err := checkMode(); err != nil {
		return err
	}
	if err := setLogLevel(); err != nil {
		return err
	}

	err := checkResolver(CurrentConfiguration.DidComm.ResolverUrl)
	if err != nil {
		Logger.Error("Resolver not available", "msg", err)
		return err
	} else {
		Logger.Info("Resolver available")
	}

	return nil
}

func IsDev() bool {
	return env == ENV_DEV
}

func IsProd() bool {
	return env == ENV_PROD
}

func IsForwardTypeNats() bool {
	return CurrentConfiguration.CloudForwarding.Protocol == NATS
}

func IsForwardTypeHybrid() bool {
	return CurrentConfiguration.CloudForwarding.Protocol == HYBRID
}

func readConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	viper.SetEnvPrefix("BOILERPLATE")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		var configFileNotFoundError viper.ConfigFileNotFoundError
		if errors.As(err, &configFileNotFoundError) {
			slog.Warn("Configuration not found but environment variables will be taken into account.")
		}
	}
	viper.AutomaticEnv()
}

func setDefaults() {
	viper.SetDefault("env", ENV_DEV)
	viper.SetDefault("logLevel", "info")
	viper.SetDefault("port", 9090)
	viper.SetDefault("url", "http://localhost:9090")
	viper.SetDefault("cloudForwarding.type", "http")
	viper.SetDefault("didcomm.messageEncrypted", false)
}

func setEnvironment() {
	switch strings.ToUpper(CurrentConfiguration.Env) {
	case ENV_DEV:
		env = ENV_DEV
	case ENV_PROD:
		env = ENV_PROD
	default:
		env = ENV_DEV
	}
}

func setLogLevel() (err error) {
	logLevel := new(slog.LevelVar)
	switch strings.ToLower(CurrentConfiguration.LogLevel) {
	case "debug":
		logLevel.Set(slog.LevelDebug)
	case "info":
		logLevel.Set(slog.LevelInfo)
	case "warn":
		logLevel.Set(slog.LevelWarn)
	case "error":
		logLevel.Set(slog.LevelError)
	default:
		logLevel.Set(slog.LevelWarn)
	}

	if CurrentConfiguration.Env == ENV_PROD {
		// Create a folder named "logs" if it doesn't exist
		err := os.MkdirAll("logs", 0755)
		if err != nil {
			return err
		}
		fileName := filepath.Join("logs", "log_"+strconv.FormatInt(time.Now().Unix(), 10)+".log")
		file, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		CurrentConfiguration.LoggerFile = file

		Logger = slog.New(slog.NewJSONHandler(file, &slog.HandlerOptions{
			Level: logLevel,
		}))
		Logger.Info(fmt.Sprintf("log level set to %s", logLevel.Level().String()))
	} else {
		Logger = slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level: logLevel,
		}))
		Logger.Info(fmt.Sprintf("log level set to %s", logLevel.Level().String()))
	}

	return nil
}

func checkMode() error {
	selectedTyp := CurrentConfiguration.CloudForwarding.Protocol
	switch strings.ToLower(selectedTyp) {
	case HTTP:
	case NATS:
	case HYBRID:
		return fmt.Errorf("selected mode %s not yet supported", selectedTyp)
	default:
		return fmt.Errorf("unknown cloud forwarding type %s. Select one of these types: %s, %s or %s", selectedTyp, HTTP, NATS, HYBRID)
	}

	return nil
}

func checkResolver(resolverUrl string) error {
	queryUrl, err := url.JoinPath(resolverUrl, "/health")
	if err != nil {
		return err
	}
	resp, err := http.Get(queryUrl)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("resolver not available")
	}
	return nil
}
