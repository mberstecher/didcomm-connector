package protocol_test

import (
	"gaiax/didcommconnector/internal/config"
	"gaiax/didcommconnector/mediator"
	"gaiax/didcommconnector/protocol"
	"log/slog"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func init() {
	config.CurrentConfiguration.Database.InMemory = true
	med = mediator.NewMediator(slog.Default())
	tp = protocol.NewTrustPing(med)
}

func TestHandleMessage_MessageExpired(t *testing.T) {
	// Sample data
	now := uint64(time.Now().Unix())
	nwoStr := strconv.FormatUint(now, 10)
	bodyString := "" +
		"{" +
		"\"id\": \"123456789abcdefghi\"," +
		"\"type\": \"type\"," +
		"\"body\":\"{}\"," +
		"\"from\": \"did:from\"," +
		"\"to\": [" +
		"\"did:to\"" +
		"]," +
		"\"created_time\": " + nwoStr + "," +
		"\"expires_time\": " + nwoStr + "," +
		"\"attachments\": []" +
		"}"

	packedMsg, err := protocol.HandleMessage(bodyString, med)

	// Check if the result matches the expected outcome
	prType := "https://didcomm.org/report-problem/2.0/problem-report"
	prComment := "Message has expired"

	assert.Equal(t, nil, err)
	assert.Contains(t, packedMsg, prType, prComment)
}

func TestHandleMessage_MessageCreatedWrong(t *testing.T) {

	now := time.Now()
	future := uint64(now.Add(time.Hour * 10).Unix())
	futureStr := strconv.FormatUint(future, 10)
	// Sample data
	bodyString := "" +
		"{" +
		"\"id\": \"123456789abcdefghi\"," +
		"\"type\": \"type\"," +
		"\"body\":\"{}\"," +
		"\"from\": \"did:from\"," +
		"\"to\": [" +
		"\"did:to\"" +
		"]," +
		"\"created_time\": " + futureStr + "," +
		"\"expires_time\": " + futureStr + "," +
		"\"attachments\": []" +
		"}"

	packedMsg, err := protocol.HandleMessage(bodyString, med)

	// Check if the result matches the expected outcome
	prType := "https://didcomm.org/report-problem/2.0/problem-report"
	prComment := "Message creation time is in the future"

	assert.Equal(t, nil, err)
	assert.Contains(t, packedMsg, prType)
	assert.Contains(t, packedMsg, prComment)
}
