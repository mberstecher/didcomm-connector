package secretsresolver

import "gaiax/didcommconnector/didcomm"

// Is needed to have the store functionallity
type Adapter interface {
	GetSecret(secretid string, cb *didcomm.OnGetSecretResult) didcomm.ErrorCode
	FindSecrets(secretids []string, cb *didcomm.OnFindSecretsResult) didcomm.ErrorCode
	StoreSecret(secret didcomm.Secret) error
}
