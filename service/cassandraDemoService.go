package service

import (
	"fmt"
	"gaiax/didcommconnector/internal/config"
	"gaiax/didcommconnector/model"

	"github.com/gocql/gocql"
)

type CassandraService struct {
	session *gocql.Session
}

func (c *CassandraService) GetById(id string) (con model.Connection, err error) {

	defer func() {
		c.session.Close()
	}()

	c.session = initSession()

	query := "SELECT * FROM dcc WHERE id = ? ;"
	if err = c.session.Query(query, id).Scan(&con.Id, &con.Did); err != nil {
		fmt.Println("Error while getting by id. ", err)
		return
	}
	fmt.Println("Selected object: ", con)
	return con, nil
}

func (c *CassandraService) GetAll() (cons []model.Connection, err error) {
	defer func() {
		c.session.Close()
	}()

	c.session = initSession()

	var con model.Connection
	query := "SELECT * FROM dcc;"
	m := map[string]interface{}{}
	iter := c.session.Query(query).Iter()
	for iter.MapScan(m) {
		con = model.Connection{
			Id:  m["id"].(gocql.UUID),
			Did: m["did"].(string),
		}
		cons = append(cons, con)
		m = map[string]interface{}{}
	}
	fmt.Println("result: ", cons)
	return cons, nil
}

func (c *CassandraService) Insert(did string) (id gocql.UUID, err error) {
	defer func() {
		c.session.Close()
	}()

	c.session = initSession()
	uuid, err := gocql.RandomUUID()

	query := "INSERT INTO dcc(id, did) VALUES (?, ?);"
	if err = c.session.Query(query, uuid, did).Exec(); err != nil {
		fmt.Println("Error while inserting. ", err)
		return
	}
	return uuid, nil
}

func (c *CassandraService) Update(con model.Connection) (err error) {
	defer func() {
		c.session.Close()
	}()
	fmt.Println(con)
	c.session = initSession()
	query := "UPDATE dcc SET name=? WHERE id=? ;"
	if err = c.session.Query(query, con.Did, con.Did).Exec(); err != nil {
		fmt.Println("Error while updating. ", err)
		return
	}
	return nil
}

func (c *CassandraService) DeleteDid(did string) (err error) {
	defer func() {
		c.session.Close()
	}()

	c.session = initSession()
	query := "DELETE FROM dcc WHERE did = ? ;"
	if err = c.session.Query(query, did).Exec(); err != nil {
		fmt.Println("Error while deleting. ", err)
		return
	}
	return nil
}

func (c *CassandraService) Delete(id gocql.UUID) (err error) {
	defer func() {
		c.session.Close()
	}()

	c.session = initSession()
	query := "DELETE FROM dcc WHERE id = ? ;"
	if err = c.session.Query(query, id).Exec(); err != nil {
		fmt.Println("Error while deleting. ", err)
		return
	}
	return nil
}

func initSession() *gocql.Session {
	fmt.Println("Database session: Start initializing")

	// todo init keyspace script on start
	// cluster := gocql.NewCluster("172.18.0.2")
	// cluster := gocql.NewCluster("localhost")

	cluster := gocql.NewCluster("localhost")
	cluster.Port = config.CurrentConfiguration.Database.Port
	cluster.Keyspace = config.CurrentConfiguration.Database.Keyspace

	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: config.CurrentConfiguration.Database.User,
		Password: config.CurrentConfiguration.Database.Password,
	}

	session, err := cluster.CreateSession()
	if err != nil {
		fmt.Println("Error while creating a DB session: ", err.Error())
	}

	fmt.Println("Initialize database session: Initialized")
	return session
}
